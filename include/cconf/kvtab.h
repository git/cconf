
/*
  kvtab.h -- Define simpler functions for handling hash list
  Copyright (C) 2017-2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KVTAB_H
#define KVTAB_H

#include <stdio.h>
#include <stdlib.h>
#include <cconf/types.h>

// Actual structs defined in src/kvtab-private.h
typedef struct _kv    kv_t;
typedef struct _kvtab lcc_kvtab_t;

lcc_kvtab_t *
lcc_kvtab_new(void);

int
lcc_kvtab_open(lcc_kvtab_t *, const char *);

int
lcc_kvtab_add(lcc_kvtab_t *, char *, void *, enum dtype);

double
lcc_kvtab_get_num(lcc_kvtab_t *, char *);

char *
lcc_kvtab_get_str(lcc_kvtab_t *, char *);

void
lcc_kvtab_deinit(lcc_kvtab_t *);

#endif // KVTAB_H
