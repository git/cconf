
/*
  types.h -- Forward Declarations
  Copyright (C) 2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CONF_TYPES_H
#define CONF_TYPES_H

typedef enum dtype
  {
    D_INT,
    D_STR,
    D_BOOL
  } dtype_t;

struct _kv;
struct _kvtab;
struct _cconf;
struct cc_err;

#endif // CONF_TYPES_H
