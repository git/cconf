
/*
  cconf.h -- Declaration of actual API functions and types
  Copyright (C) 2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CCONF_H
#define CCONF_H

#include <cconf/types.h>
#include <cconf/kvtab.h>
#include <cconf/config.h>
#include <stdio.h>
#ifdef WITH_PTHREAD
# include <pthread.h>
#endif

typedef struct _cconf
{
  lcc_kvtab_t    *kvtab;    // key-value table
  FILE           *create_f; // If creating a file, use this file pointer
  char           *path;     // path to config file
} cconf_t;

cconf_t *
cconf_new(void);

int
cconf_open(cconf_t *, char *);

int
cconf_update(cconf_t *);

int
cconf_create(cconf_t *, char *);

int
cconf_add_str(cconf_t *, char *, char *);

int
cconf_add_num(cconf_t *, char *, double);

double
cconf_get_num(cconf_t *, char *);

char *
cconf_get_str(cconf_t *, char *);

void
cconf_free(cconf_t *);

#endif // CCONF_H
