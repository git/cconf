
/*
  error.h -- Used for handling errors in cconf
  Copyright (C) 2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ERROR_H
#define ERROR_H

// Project specific error status
struct cc_err
{
  int   no;
  int   line;
  char *file;
  char *func;
};

extern struct cc_err cc_err;

void
set_err(char *, char *, int, int);

/* Get the associated error string to an issue */
const char *
cconf_err_str(void);

/* Print the associated error */
void
cconf_error(void);

#endif // ERROR_H
