
/*
  cconf_session.c -- Declare api functions for creating and destorying sessions
  Copyright (C) 2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cconf/cconf.h>
#include <cconf/kvtab.h>
#include <cconf/types.h>
#include <cconf/error.h>
#include <cconf/config.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#ifdef WITH_PTHREAD
# include <pthread.h>
#endif
#include <signal.h>
#include "kvtab-private.h"

struct cc_err cc_err; // Declared in cconf/error.h

cconf_t *
cconf_new(void)
{
  // Set default error info
  cc_err.no   = 0;
  cc_err.file = NULL;
  cc_err.line = 0;
  cc_err.func = NULL;

  cconf_t *tmp = malloc(sizeof(cconf_t));
  if (!tmp)
    {
      set_err("cconf_session", "cconf_new", 43, 3);
      return NULL;
    }

  // Initiate the internals
  tmp->kvtab   = lcc_kvtab_new();
  if (!tmp->kvtab)
    {
      set_err("cconf_session", "cconf_new", 51, 3);
      free(tmp);
      return NULL;
    }

  tmp->path     = NULL;
  tmp->create_f = NULL;

  return tmp;
}

void
cconf_free(cconf_t *cconf)
{
  if (!cconf)
    return;

  // Restore the file permissions
  //chmod(cconf->path, cconf->kvtab->stat.st_mode);

  // De-initiate
  lcc_kvtab_deinit(cconf->kvtab);

  if (cconf->create_f)
    fclose(cconf->create_f);

  free(cconf);
}
