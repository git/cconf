
/*
  kvtab.c -- Implementation of kvtab
  Copyright (C) 2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cconf/config.h>

#include <cconf/types.h>
#include <cconf/kvtab.h>
#include <cconf/error.h>
#include <gl_list.h>
#include <gl_array_list.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>
#include <math.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include "kvtab-private.h"

struct cc_err cc_err;

static _Bool
kv_compare(const void *str, const void *__kv)
{
  // FIXME: This may be in the wrong order
  struct _kv *kv = (struct _kv *) __kv;

  // compare key to string
  return !strcmp((const char *) str, (const char *) kv->key);
}

/* Implement kv_t functions */
struct _kv *
kv_new(char *key, void *val, dtype_t type)
{
  struct _kv *kv = malloc(sizeof(struct _kv));
  if (!kv)
    {
      set_err("kvtab", "kv_new", 108, 3);
      return NULL;
    }

  // Set key value
  kv->key = malloc(strlen(key));
  if (!kv->key)
    {
      set_err("kvtab", "kv_new", 116, 3);
      return NULL;
    }
  memcpy(kv->key, key, strlen(key));
  kv->key[strlen(key)] = '\0';

  if (type == D_STR)
    {
      kv->val = malloc(strlen(val));
      if (!kv->val)
        {
          set_err("kvtab", "kv_new", 127, 3);
          free(kv->key);
          return NULL;
        }

      memcpy(kv->val, val, strlen(val));
      char *cptr = kv->val;
      cptr[strlen(val)] = '\0'; // Quick little conversion to set null character
      cptr = NULL;
    }
  else if (type == D_INT || type == D_BOOL)
    {
      kv->val = malloc(sizeof(double));
      if (!kv->val)
        {
          set_err("kvtab", "kv_new", 142, 3);
          free(kv->key);
          return NULL;
        }

      memcpy(kv->val, val, sizeof(double));
    }
  else
    {
      // BOOL falls under number type
      set_err("kvtab", "kv_new", 152, 4);
      free(kv->key);
      return NULL;
    }

  kv->type = type;

  return kv; // return created struct
}

double
kv_get_num(struct _kv *kv)
{
  if (kv->type != D_INT)
    {
      set_err("kvtab", "kv_get_num", 167, 5);
      // This is the max double value, so it's a unique error
      return -(pow(2, 53));
    }

  return *((double *) kv->val); // dereference double from value
}

char *
kv_get_str(struct _kv *kv)
{
  if (!kv->type == D_STR)
    {
      set_err("kvtab", "kv_get_str", 180, 5);
      return NULL;
    }

  return (char *) kv->val; // Not needed cast, but whatever
}

void
kv_free(const void *val)
{
  kv_t *kv = (kv_t *) val;
  free(kv->key);
  free(kv->val);
  free(kv);
}

/* kv_t done */

lcc_kvtab_t *
lcc_kvtab_new(void)
{
  lcc_kvtab_t *kvtab = malloc(sizeof(lcc_kvtab_t));
  if (!kvtab)
    {
      set_err("kvtab", "lcc_kvtab_new", 204, 3);
      return NULL;
    }

  kvtab->kv = gl_list_nx_create_empty(GL_ARRAY_LIST,
                                      kv_compare,     // custom compare func
                                      NULL,
                                      kv_free,        // custom free func
                                      false);

  if (!kvtab->kv)
    {
      set_err("kvtab", "lcc_kvtab_new", 216, 3);
      free(kvtab);
      return NULL;
    }

  kvtab->file = NULL;

  return kvtab;
}

int
lcc_kvtab_open(lcc_kvtab_t *tab, const char *path)
{
  int ret;

  tab->file = fopen(path, "r+"); // open file in read and write mode

  if (!tab->file)
    {
      set_err("kvtab", "lcc_kvtab_open", 233, 2);
      return -2;
    }

  ret = stat(path, &tab->stat);
  if (ret < 0)
    {
      set_err("kvtab", "lcc_kvtab_open", 193, 9);
      return -9;
    }

#if 0
  /*
    To prevent the file from being changed will it is in use,
    change the permission to R/O. Make sure to change it back
    to original permissions when done
   */
  ret = chmod(path, S_IRUSR | S_IRGRP | S_IROTH);
  if (ret < 0)
    {
      set_err("kvtab", "lcc_kvtab_open", 200, 9);
      return -9;
    }
#endif

  return 0;
}

int
lcc_kvtab_add(lcc_kvtab_t *tab, char *key, void *val, dtype_t type)
{
  int ret;

  // If any parameter is null, then return
  if (!tab || !key || !val)
    {
      set_err("kvtab", "lcc_kvtab_add", 246, 1);
      return -1;
    }

  // Check for duplicates
  gl_list_node_t duplicate_node = gl_list_search(tab->kv, key);
  if (duplicate_node)
    {
      const void *nval = gl_list_node_value(tab->kv, duplicate_node);

      // Get a kv pair
      kv_t *kv = (kv_t *) nval;

      if (type == D_STR)
        {
          char *str = kv_get_str(kv);
          if (!strcmp(str, (char *) val))
            return 0;
          else
            kv->val = val;
        }
      else
        {
          double nval = kv_get_num(kv);
          double pval = *((double *) val);

          if (pval == nval)
            return 0;
          else
            {
              kv->val = val;
              return 0;
            }
        }
    }

  // Create a new key value pair
  kv_t *kv = kv_new(key, val, type);
  if (!kv)
    {
      set_err("kvtab", "lcc_kvtab_add", 254, 9);
      return -9;
    }

  // Place the kv node in the last spot
  gl_list_node_t node = gl_list_nx_add_last(tab->kv, kv);
  if (!node)
    {
      set_err("kvtab", "lcc_kvtab_add", 262, 6);
      return -6;
    }

  return 0;
}

double
lcc_kvtab_get_num(lcc_kvtab_t *tab, char *key)
{
  if (!tab || !key)
    {
      set_err("kvtab", "lcc_kvtab_get_num", 274, 1);
      return -(pow(2, 53));
    }

  // Find the node
  gl_list_node_t node = gl_list_search(tab->kv, key);
  if (!node)
    {
      set_err("kvtab", "lcc_kvtab_get_num", 282, 7);
      return -(pow(2, 53));
    }

  // Get the data from the found node
  kv_t *kv = (kv_t *) gl_list_node_value(tab->kv, node);
  if (!kv)
    {
      set_err("kvtab", "lcc_kvtab_get_num", 290, 7);
      return -(pow(2, 53));
    }

  return kv_get_num(kv);
}

char *
lcc_kvtab_get_str(lcc_kvtab_t *tab, char *key)
{
  if (!tab || !key)
    {
      set_err("kvtab", "lcc_kvtab_get_str", 301, 1);
      return NULL;
    }

  gl_list_node_t node = gl_list_search(tab->kv, key);
  if (!node)
    {
      set_err("kvtab", "lcc_kvtab_get_str", 309, 7);
      return NULL;
    }

  // Get the data from the found node
  kv_t *kv = (kv_t *) gl_list_node_value(tab->kv, node);
  if (!kv)
    {
      set_err("kvtab", "lcc_kvtab_set_str", 317, 7);
      return NULL;
    }

  return kv_get_str(kv);
}

void
lcc_kvtab_deinit(lcc_kvtab_t *tab)
{
  if (tab->file)
    fclose(tab->file);

  gl_list_free(tab->kv);
  free(tab);
}
