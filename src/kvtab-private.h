
/*
  kvtab-private.h -- Private header of kvtab type definition
  Copyright (C) 2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KVTAB_PRIVATE_H
#define KVTAB_PRIVATE_H

#include <cconf/config.h>

#include <cconf/types.h>
#include <stdio.h>
#include <gl_list.h>
#include <sys/stat.h>
#include <sys/types.h>

struct _kvtab
{
  FILE       *file; // config file
  struct stat stat;
  gl_list_t   kv;   // list contains a struct of key-values
};

struct _kv
{
  char   *key;   // key of pair
  void   *val;   // value of key
  dtype_t type;  // value type
};

// A few functions for struct _kv

struct _kv*
kv_new(char *, void *, enum dtype);

double
kv_get_num(struct _kv *);

char *
kv_get_str(struct _kv *);

void
kv_free(const void *);

#endif // KVTAB_PRIVATE_H
