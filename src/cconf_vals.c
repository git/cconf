
/*
  cconf_vals.c -- Used for getting and setting pairs in the session
  Copyright (C) 2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cconf/kvtab.h>
#include <cconf/error.h>
#include <cconf/types.h>
#include <cconf/cconf.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int
cconf_add_str(cconf_t *cconf, char *key, char *val)
{
  // Make sure that all parameters are non-null
  if (!cconf->create_f || !cconf || !key || !val)
    {
      set_err("cconf_vals", "cconf_add_str", 35, 1);
      return -1;
    }

  // Add the pair to the table
  int ret;
  ret = lcc_kvtab_add(cconf->kvtab, key, val, D_STR);
  if (ret < 0)
    return ret;

  // Just write the data directly, don't make a string and then write it
  fprintf(cconf->create_f, "%s = %s\n", key, val);

  return 0;
}

int
cconf_add_num(cconf_t *cconf, char *key, double val)
{
  // Make sure that all parameters are non-null
  if (!cconf->create_f || !cconf || !key)
    {
      set_err("cconf_vals", "cconf_add_str", 35, 1);
      return -1;
    }

  int ret;
  ret = lcc_kvtab_add(cconf->kvtab, key, &val, D_INT);
  if (ret < 0)
    return ret;

  // Yet again, write the data directly
  fprintf(cconf->create_f, "%s = %.2f\n", key, val);

  return 0;
}

double
cconf_get_num(cconf_t *cconf, char *key)
{
  return lcc_kvtab_get_num(cconf->kvtab, key);
}

char *
cconf_get_str(cconf_t *cconf, char *key)
{
  return lcc_kvtab_get_str(cconf->kvtab, key);
}
