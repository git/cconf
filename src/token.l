
%{
/*
  token.l -- Tokenizer for file parsing
  Copyright (C) 2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cconf/kvtab.h>
#include <cconf/types.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "parser.h"

extern int yylineno;

%}

%option noyywrap yylineno

%%

"#"[^\n]*                                   { /* Ignore comments */ }

"true"  |
"True"  |
"TRUE"                                      { yylval.bval = 1; return BOOL;  }
"false" |
"False" |
"FALSE"                                     { yylval.bval = 0; return BOOL;  }

-?([0-9]+|[0-9]*\.[0-9]+([eE][-+]?[0-9]+)?) {
                                              yylval.num = atof(yytext);
                                              return NUM;
                                            }

\"([^\\\"]|\\.)*\"                          {
                                              char str[strlen(yytext)];
                                              memcpy(str,
                                                     yytext + 1,
                                                     strlen(yytext - 2));
                                              str[strlen(str) - 1] = '\0';
                                              yylval.str = strdup(str);
                                              return STR;
                                            }


[a-zA-Z_]+                                  {
                                              yylval.str = strdup(yytext);
                                              return STR;
                                            }
[ \t]+                                      { /* Ignore whitespace */ }
"="                                         { return ASGN;  }
"\n"                                        { return EOL;   }
.                                           {
                                              printf("Unknown char: %c\n",
                                                     yytext[0]);
                                            }
%%

extern void
parser_setup(void)
{
    yylineno = 1;
}
