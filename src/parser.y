
%{
/*
  parser.y -- Bison file parser
  Copyright (C) 2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cconf/kvtab.h>
#include <cconf/types.h>
#include <cconf/error.h>
#include <stdbool.h>
#include <stdio.h>

int ret;

int yylineno; // defined in token.l
int cc_errno; // defined in include/cconf/error.h

%}

%parse-param {lcc_kvtab_t **tab}

%union {
    char        *str;  // String for key or value
    double       num;  // number as value
    unsigned int bval; // boolean value
}

%type   <str>           STR
%type   <num>           NUM
%type   <bval>          BOOL
%token                  STR
%token                  NUM
%token                  BOOL
%token                  ASGN
%token                  BQ
%token                  LQ
%token                  EOL


%start file

%%

file:           /* Nothing */
        |       file def
        ;

def:            EOL               { /*  Do nothing on empty lines      */ }
        |       STR ASGN STR  EOL { ret = lcc_kvtab_add(*tab, $1,  $3, D_STR); }
        |       STR ASGN NUM  EOL { ret = lcc_kvtab_add(*tab, $1, &$3, D_INT); }
        |       STR ASGN BOOL EOL { ret = lcc_kvtab_add(*tab, $1, &$3, D_BOOL);}
        ;

%%

void
yyerror(char *str)
{
    cc_errno = -8;
    fprintf(stderr, "cconf parsing error (line %d): %s\n", yylineno, str);
}
