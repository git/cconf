
/*
  cconf_file.c -- Functions for handling files
  Copyright (C) 2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cconf/types.h>
#include <cconf/kvtab.h>
#include <cconf/cconf.h>
#include <cconf/error.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <md5.h>
//#include "parser.h"
#include "kvtab-private.h"

struct cc_err cc_err; // Defined in include/cconf/error.h

// Defined in token.l
void
parser_setup(void);

void
yyrestart(FILE *);

int
cconf_open(cconf_t *cconf, char *path)
{
  int ret;

  if (!cconf)
    {
      set_err("cconf_file", "cconf_open", 61, 1);
      return -1;
    }

  // Set path
  if (path)
    cconf->path = strdup(path);
  else
    {
      set_err("cconf_file", "cconf_open", 72, 1);
      return -1;
    }

  // open the file
  ret = lcc_kvtab_open(cconf->kvtab, path);
  if (ret < 0)
    return ret; // Should already be negative

  // Open the file
  yyrestart(cconf->kvtab->file);

  // External function declared in token.l
  parser_setup();

  // Parse file
  ret = yyparse(&cconf->kvtab);
  if (ret)
    {
      set_err("cconf_file", "cconf_open", 77, 8);
      return -8;
    }

  return 0; // File is opened and the path is copied
}

int
cconf_update(cconf_t *cconf)
{
  int ret;

  // Rewind the file that is to be parsed
  rewind(cconf->kvtab->file);

  yyrestart(cconf->kvtab->file);
  parser_setup();
  ret = yyparse(&cconf->kvtab);
  if (ret)
    {
      set_err("cconf_file", "cconf_reread", 92, 8);
      return -8;
    }

  return 0;
}
