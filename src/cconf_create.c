
/*
  cconf_create.c -- Used for creating a file from added values
  Copyright (C) 2018  Charlie Sale

  This file is part of cconf

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <cconf/cconf.h>
#include <cconf/kvtab.h>
#include <cconf/error.h>
#include <cconf/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "kvtab-private.h"

int
cconf_create(cconf_t *cconf, char *path)
{
  cconf->create_f = fopen(path, "w+");
  if (!cconf->create_f)
    {
      set_err("cconf_create", "cconf_create", 49, 10);
      return -10;
    }

  return 0;
}
